﻿using System;
using System.Collections.Generic;
using System.Linq;
using Malee.List;
using TopAdventure.Interfaces;
using TopAdventure.Managers;
using UnityEngine;

namespace TopAdventure.ECS
{
    public abstract class Entity : MonoBehaviour, ITick
    {
        public string currentState;
        [Reorderable] public CustomBehavioursList behavioursList;
        public readonly List<Data> dataList = new List<Data>();
        public event Action BehavioursListChanged;

        public delegate void BehavioursList();

        public event BehavioursList BehavioursListRefreshed;

        [System.Serializable]
        public class CustomBehavioursList : ReorderableArray<CustomBehaviour>
        {
        }

        private int previousBehavioursListCount;

        private readonly CustomBehavioursList previousBehavioursList = new CustomBehavioursList();

        public void Tick()
        {
            CheckBehavioursListState();
        }

        public void SetNewBehaviours()
        {
            BehavioursListRefreshed?.Invoke();
        }

        protected void Initialize()
        {
            BehavioursListRefreshed += RefreshBehavioursNotify;
            FillingPreviousBehavioursList();
            previousBehavioursListCount = behavioursList.Count;
            SendEntityInstanceToBehaviours(this);
            ManagerUpdate.AddTo(this);
        }

        protected void WriteCollectedData(params Data[] dataVariables)
        {
            foreach (var currentData in dataVariables)
                dataList.Add(currentData);
        }

        private void SendEntityInstanceToBehaviours(Entity currentEntity)
        {
            foreach (var behaviour in behavioursList)
            {
                if (ValidateBehaviour(behaviour))
                    behaviour.PrimaryInitializeBehaviour(currentEntity);
            }
        }

        private void RefreshBehavioursNotify()
        {
            foreach (var behaviour in behavioursList)
            {
                behaviour.RefreshBehaviours();
            }
        }

        private bool ValidateBehaviour(CustomBehaviour customBehaviour)
        {
            if (customBehaviour == null)
            {
                Debug.Log("Please set all specific behaviour to behavioursList");
                return false;
            }

            return true;
        }

        private void CheckBehavioursListState()
        {
            var isBehavioursCountChanged = false;
            var isBehavioursValuesChanged = false;

            CheckBehavioursListCount(ref isBehavioursCountChanged);
            CheckBehavioursListValues(ref isBehavioursValuesChanged, isBehavioursCountChanged);
            if (isBehavioursCountChanged || isBehavioursValuesChanged)
            {
                BehavioursListChanged?.Invoke();
            }
        }

        private void CheckBehavioursListCount(ref bool isCountChanged)
        {
            if (behavioursList.Count == previousBehavioursListCount) return;
            isCountChanged = true;
            previousBehavioursListCount = behavioursList.Count;
        }

        private void CheckBehavioursListValues(ref bool isValuesChanged, bool isCountChanged)
        {
            if (isCountChanged == false && behavioursList.Count > 0)
                if (previousBehavioursList.Where((customBehaviour, behaviourNumber) =>
                    customBehaviour != behavioursList[behaviourNumber]).Any())
                    isValuesChanged = true;

            FillingPreviousBehavioursList();
        }

        private void FillingPreviousBehavioursList()
        {
            previousBehavioursList.Clear();

            foreach (var behaviour in behavioursList)
                previousBehavioursList.Add(behaviour);
        }
    }
}