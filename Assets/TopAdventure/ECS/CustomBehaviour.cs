﻿using TopAdventure.Interfaces;
using TopAdventure.Managers;
using UnityEngine;

// [CreateAssetMenu(fileName = "CustomBehaviour", menuName = "CustomBehaviour")]
namespace TopAdventure.ECS
{
    public abstract class CustomBehaviour : ScriptableObject, IEventSub
    {
        protected abstract void Initialize();
        public abstract void Subscribe();
        public abstract void UnSubscribe();

        // protected abstract void ClearModule();
        protected Entity entityInstance;

        public void PrimaryInitializeBehaviour(Entity currentEntity)
        {
            entityInstance = currentEntity;
            Initialize();
            Subscribe();
            CheckAllData();
            ManagerUpdate.AddTo(this);
        }
        
        public void RefreshBehaviours()
        {
            Subscribe();
            entityInstance.BehavioursListChanged += ShutdownCurrentBehaviourModule;
            ManagerUpdate.AddTo(this);
        }
        

        private void CheckAllData()
        {
            if (entityInstance.dataList.Count == 0)
                Debug.Log($"no data was found in the current entity: {entityInstance.GetType()}");
        }

        private void ShutdownCurrentBehaviourModule()
        {
            UnSubscribe();
            ManagerUpdate.RemoveFrom(this);
            // ClearModule();
            entityInstance.BehavioursListChanged -= ShutdownCurrentBehaviourModule;
            entityInstance.SetNewBehaviours();
        }
    }
}