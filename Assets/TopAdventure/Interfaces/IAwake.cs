﻿namespace TopAdventure.Interfaces
{
    public interface IAwake
    {
        void OnAwake();

    }
}
