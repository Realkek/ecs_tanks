﻿namespace TopAdventure.Interfaces
{
    public interface IStart
    {
        void OnStart();
    }
}
