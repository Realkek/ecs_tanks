﻿namespace TopAdventure.Interfaces
{
    public interface IEventSub
    {
        void Subscribe();
        void UnSubscribe();
    }
}