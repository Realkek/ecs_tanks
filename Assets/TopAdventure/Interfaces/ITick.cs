﻿namespace TopAdventure.Interfaces
{
    public interface ITick
    {
        void Tick();
    }
}
