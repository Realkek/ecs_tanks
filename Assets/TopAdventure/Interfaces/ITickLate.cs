﻿namespace TopAdventure.Interfaces
{
    public interface ITickLate
    {
        void TickLate();
    }
}
