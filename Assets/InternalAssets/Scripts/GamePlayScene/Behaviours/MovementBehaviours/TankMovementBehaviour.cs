using InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses;
using TopAdventure.ECS;
using UnityEngine;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours.MovementBehaviours
{
    [CreateAssetMenu(fileName = "TankMovement", menuName = "CustomBehaviours/Movement/TankMovement")]
    public class TankMovementBehaviour : CustomBehaviour
    {
        private TankMovementDataCash tankMovementDataCash; 
        protected override void Initialize()
        {
            tankMovementDataCash = new TankMovementDataCash(entityInstance.dataList);
        }
    
        public override void Subscribe()
        {
            if (tankMovementDataCash.MovementData != null)
            {
                tankMovementDataCash.MovementData.ForwardDirectionReceived += MoveForward;
                tankMovementDataCash.MovementData.LeftDirectionReceived += MoveLeft;
                tankMovementDataCash.MovementData.BackDirectionReceived += MoveBack;
                tankMovementDataCash.MovementData.RightDirectionReceived += MoveRight;
            }
        }

        public override void UnSubscribe()
        {
            if (tankMovementDataCash.MovementData != null)
            {
                tankMovementDataCash.MovementData.ForwardDirectionReceived -= MoveForward;
                tankMovementDataCash.MovementData.LeftDirectionReceived -= MoveLeft;
                tankMovementDataCash.MovementData.BackDirectionReceived -= MoveBack;
                tankMovementDataCash.MovementData.RightDirectionReceived -= MoveRight;
            }
        }
        

        private  void MoveForward()
        {
            entityInstance.transform.Translate(0,0,0.1f);
        }
        private  void MoveLeft()
        {
            entityInstance.transform.Translate(-0.1f,0,0);
        }
        private  void MoveBack()
        {
            entityInstance.transform.Translate(0,0,-0.1f);
        }
        private  void MoveRight()
        {
            entityInstance.transform.Translate(0.1f,0,0);
        }
    }
}
