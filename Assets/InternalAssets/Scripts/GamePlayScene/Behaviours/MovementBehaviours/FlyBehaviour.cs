﻿using System;
using System.Collections;
using System.Collections.Generic;
using TopAdventure.ECS;
using TopAdventure.Interfaces;
using UnityEngine;

[CreateAssetMenu(fileName = "Fly", menuName = "CustomBehaviours/FlyBehaviour")]
public class FlyBehaviour : CustomBehaviour, ITick
{
    protected override void Initialize()
    {
    }

    public void Tick()
    {
    }
    
    public override void Subscribe()
    {
    }

    public override void UnSubscribe()
    {
    }
    
}