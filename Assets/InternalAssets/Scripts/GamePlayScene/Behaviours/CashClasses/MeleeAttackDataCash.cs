﻿using System.Collections.Generic;
using InternalAssets.Scripts.GamePlayScene.DataStore;
using TopAdventure.ECS;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses
{
    public class MeleeAttackDataCash
    {
        public MeleeAttackData MeleeAttackData { get; private set; }
        public DetectingData DetectingData { get; private set; }
        
        public MeleeAttackDataCash(IEnumerable<Data> dataList)
        {
            foreach (var data in dataList)
            {
                switch (data)
                {
                    case MeleeAttackData meleeAttackData:
                        MeleeAttackData = meleeAttackData;
                        break;
                    case DetectingData detectingData:
                        DetectingData = detectingData;
                        break;
                }
            }
           
        }
    }
}