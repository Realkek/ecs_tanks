﻿using System.Collections.Generic;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses
{
    using DataStore;
    using TopAdventure.ECS;

    public class PursueDataCash
    {
        public DetectingData DetectingData { get; private set; }
        public PursueData PursueData { get; private set; }

        public PursueDataCash(IEnumerable<Data> dataList)
        {
            foreach (var data in dataList)
            {
                switch (data)
                {
                    case PursueData pursueData:
                        PursueData = pursueData;
                        break;
                    case DetectingData detectingData:
                        DetectingData = detectingData;
                        break;
                }
            }
        }
    }
}