﻿using System.Collections.Generic;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses
{
    using DataStore;
    using TopAdventure.ECS;

    public class DetectingDataCash
    {
        public DetectingData DetectingData { get; private set; }
        public MeleeAttackData MeleeAttackData { get; private set; }

        public DetectingDataCash(IEnumerable<Data> dataList)
        {
            foreach (var data in dataList)
            {
                switch (data)
                {
                    case DetectingData detectingData:
                        DetectingData = detectingData;
                        break;
                    case MeleeAttackData meleeAttackData:
                        MeleeAttackData = meleeAttackData;
                        break;
                }
            }
        }
    }
}