﻿
using System.Collections.Generic;
using JetBrains.Annotations;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses
{
    using DataStore;
    using TopAdventure.ECS;

    public class TankMovementDataCash
    {
        public MovementData MovementData { get; private set; }
        public TankMovementDataCash(IEnumerable<Data> dataLIst)
        {
            foreach (var data in dataLIst)
            {
                if (data is MovementData receivedData)
                    MovementData = receivedData;
            }
            
        }
    }
}