﻿using System.Collections.Generic;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses
{
    using DataStore;
    using TopAdventure.ECS;

    public class PlayerInputDataCash
    {
        public PlayerInputData PlayerInputData { get; private set; }
        public MovementData MovementData { get; private set; }

        public PlayerInputDataCash(IReadOnlyList<Data> dataList)
        {
            foreach (var data in dataList)
            {
                switch (data)
                {
                    case PlayerInputData playerInputData:
                        PlayerInputData = playerInputData;
                        break;
                    case MovementData movementData:
                        MovementData = movementData;
                        break;
                }
            }
            

          
        }
    }
}