﻿using System.Collections;
using System.Collections.Generic;
using TopAdventure.ECS;
using UnityEngine;
using UnityEngine.Rendering;


[CreateAssetMenu(fileName = "DefaultValue", menuName = "CustomBehaviours/DefaultValueBehaviour")]
public class DefaultValueBehaviour : CustomBehaviour
{
    protected override void Initialize()
    {
    }
    
    public override void Subscribe()
    {
    }

    public override void UnSubscribe()
    {
    }
    
}