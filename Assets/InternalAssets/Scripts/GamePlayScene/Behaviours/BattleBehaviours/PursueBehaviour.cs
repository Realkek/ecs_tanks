﻿using System.Collections.Generic;
using System.Linq;
using InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses;
using TopAdventure.Interfaces;
using TopAdventure.Managers;
using UnityEngine;


namespace InternalAssets.Scripts.GamePlayScene.Behaviours.BattleBehaviours
{
    using TopAdventure.ECS;

    [CreateAssetMenu(fileName = "Pursue", menuName = "CustomBehaviours/Pursue")]
    public class PursueBehaviour : CustomBehaviour, ITick
    {
        private readonly List<PursueDataCash> pursuers = new List<PursueDataCash>();
        private PursueDataCash pursueDataCash;

        protected override void Initialize()
        {
            Subscribe();
            pursueDataCash = new PursueDataCash(entityInstance.dataList);
        }

        public void Tick()
        {
            Pursue();
        }
        
        public override void Subscribe()
        {
            pursueDataCash.DetectingData.EnemyHasBeenDetected += TakeACloserLook;
            pursueDataCash.DetectingData.EnemyHasBeenMissed += StopBeingAPursuer;
        }

        public override void UnSubscribe()
        {
            pursueDataCash.DetectingData.EnemyHasBeenDetected -= TakeACloserLook;
            pursueDataCash.DetectingData.EnemyHasBeenMissed -= StopBeingAPursuer;
        }
        

        private void TakeACloserLook(Collider collider, Entity entity, string currentName )
        {
            if (entity == null) return;
            pursuers.Add(new PursueDataCash(entity.dataList));
            ChangeStateToPursuingEnemy(entity);
        }

        private void StopBeingAPursuer(Collider collider, Entity entity, string currentName)
        {
            foreach (var pursuer in pursuers.Where(dataCash =>
                entity != null && dataCash.PursueData.Pursuer.name == entity.name))
                pursuers.Remove(pursuer);

            if (entity != null)
                ChangeStateToRelax(entity);
        }

        private void Pursue()
        {
            foreach (var pursuer in pursuers.Where(pursuer =>
                pursuer.PursueData != null && pursuer.PursueData.IsDisabled != true))
                pursuer.PursueData.NavMeshAgent.destination = pursuer.PursueData.Enemy.transform.position;
        }

        private void ChangeStateToPursuingEnemy(Entity entity)
        {
            entity.currentState = $"{entity.name}: Pursuing a player";
        }

        private void ChangeStateToRelax(Entity entity)
        {
            entity.currentState = $"{entity.name}: Relaxed";
        }
    }
}