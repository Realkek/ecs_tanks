﻿using InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses;
using TopAdventure.Interfaces;
using UnityEngine;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours.BattleBehaviours
{
    using TopAdventure.ECS;

    [CreateAssetMenu(fileName = "MeleeAttack", menuName = "CustomBehaviours/MeleeAttack")]
    public class MeleeAttackBehaviour : CustomBehaviour, ITick
    {
        private MeleeAttackDataCash meleeAttackDataCash;

        protected override void Initialize()
        {
            meleeAttackDataCash = new MeleeAttackDataCash(entityInstance.dataList);
            Subscribe();
        }

        public override void Subscribe()
        {
            meleeAttackDataCash.DetectingData.EnemyEnteredTheRadiusOfMeleeAttack += PrepareWeapon;
            meleeAttackDataCash.DetectingData.EnemyExitTheRadiusOfMeleeAttack += CoverWeapon;
        }

        public override void UnSubscribe()
        {
            meleeAttackDataCash.DetectingData.EnemyEnteredTheRadiusOfMeleeAttack -= PrepareWeapon;
            meleeAttackDataCash.DetectingData.EnemyExitTheRadiusOfMeleeAttack -= CoverWeapon;
        }
        

        private void ToAttackOfMelee()
        {
            
        }

        private void PrepareWeapon(Collider collider, Entity entity, string currentName)
        {
            Debug.Log("meleeAttack");
        }

        private void CoverWeapon(Collider collider, Entity entity, string currentName)
        {
        }

        public void Tick()
        {
            ToAttackOfMelee();
        }
    }
}