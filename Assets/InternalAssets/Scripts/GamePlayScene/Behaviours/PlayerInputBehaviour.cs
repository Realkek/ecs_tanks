using InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses;
using TopAdventure.ECS;
using TopAdventure.Interfaces;
using TopAdventure.Managers;
using UnityEngine;

namespace InternalAssets.Scripts.GamePlayScene.Behaviours
{
    [CreateAssetMenu(fileName = "PlayerInput", menuName = "CustomBehaviours/PlayerInput")]
    public class PlayerInputBehaviour : CustomBehaviour, ITick
    {
        private PlayerInputDataCash playerInputDataCash;

        protected override void Initialize()
        {
            playerInputDataCash = new PlayerInputDataCash(entityInstance.dataList);
        }

        public override void Subscribe()
        {
            if (playerInputDataCash.PlayerInputData != null)
            {
                playerInputDataCash.PlayerInputData.PlayerHoldW += TriggerForwardMove;
                playerInputDataCash.PlayerInputData.PlayerHoldA += TriggerLeftMove;
                playerInputDataCash.PlayerInputData.PlayerHoldS += TriggerBackMove;
                playerInputDataCash.PlayerInputData.PlayerHoldD += TriggerRightMove;
            }
        }

        public override void UnSubscribe()
        {
            if (playerInputDataCash.PlayerInputData != null)
            {
                playerInputDataCash.PlayerInputData.PlayerHoldW -= TriggerForwardMove;
                playerInputDataCash.PlayerInputData.PlayerHoldA -= TriggerLeftMove;
                playerInputDataCash.PlayerInputData.PlayerHoldS -= TriggerBackMove;
                playerInputDataCash.PlayerInputData.PlayerHoldD -= TriggerRightMove;
            }
        }
        

        private void TriggerForwardMove()
        {
            playerInputDataCash.MovementData.ForwardDirectionReceived?.Invoke();
        }

        private void TriggerLeftMove()
        {
            playerInputDataCash.MovementData.LeftDirectionReceived?.Invoke();
        }

        private void TriggerBackMove()
        {
            playerInputDataCash.MovementData.BackDirectionReceived?.Invoke();
        }

        private void TriggerRightMove()
        {
            playerInputDataCash.MovementData.RightDirectionReceived?.Invoke();
        }

        public void Tick()
        {
            if (Input.GetKey(KeyCode.W))
                playerInputDataCash.PlayerInputData.PlayerHoldW?.Invoke();
            if (Input.GetKey(KeyCode.A))
                playerInputDataCash.PlayerInputData.PlayerHoldA?.Invoke();
            if (Input.GetKey(KeyCode.S))
                playerInputDataCash.PlayerInputData.PlayerHoldS?.Invoke();
            if (Input.GetKey(KeyCode.D))
                playerInputDataCash.PlayerInputData.PlayerHoldD?.Invoke();
        }
    }
}