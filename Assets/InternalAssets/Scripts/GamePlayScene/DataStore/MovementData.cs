﻿using System;
using TopAdventure.ECS;

namespace InternalAssets.Scripts.GamePlayScene.DataStore
{
    [System.Serializable]
    public class MovementData : Data
    {
        public float moveSpeed;

        public Action ForwardDirectionReceived;
        public Action BackDirectionReceived;
        public Action LeftDirectionReceived;
        public Action RightDirectionReceived;
        public Action UpDirectionReceived;
        public Action DownDirectionReceived;
    }
}