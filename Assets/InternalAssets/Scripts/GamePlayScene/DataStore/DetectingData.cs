﻿using System;
using UnityEngine;

namespace InternalAssets.Scripts.GamePlayScene.DataStore
{
    using TopAdventure.ECS;

    [Serializable]
    public class DetectingData : Data
    {
        public Collider detectingBoxCollider;
        public Action<Collider, Entity, string> EnemyHasBeenDetected;
        public Action<Collider, Entity, string> EntityDetectingColliderExit;
        public Action<Collider, Entity, string> EntityDetectingColliderTriggered;
        public Action<Collider, Entity, string> EnemyHasBeenMissed;
        public Action<Collider, Entity, string> EnemyEnteredTheRadiusOfMeleeAttack;
        public Action<Collider, Entity, string> EnemyExitTheRadiusOfMeleeAttack;
    }
}