﻿using System;
using TopAdventure.ECS;

namespace InternalAssets.Scripts.GamePlayScene.DataStore
{
    [Serializable]
    public class HpData : Data
    {
        public int health;
    }
}