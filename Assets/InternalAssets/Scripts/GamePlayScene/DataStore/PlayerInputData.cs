﻿using System;
using TopAdventure.ECS;

namespace InternalAssets.Scripts.GamePlayScene.DataStore
{
    [System.Serializable]
    public class PlayerInputData : Data
    {
        public Action PlayerHoldW;
        public Action PlayerHoldA;
        public Action PlayerHoldS;
        public Action PlayerHoldD;
    }
}