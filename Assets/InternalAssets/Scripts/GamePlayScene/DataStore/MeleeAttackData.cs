﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InternalAssets.Scripts.GamePlayScene.DataStore
{
    using TopAdventure.ECS;

    [Serializable]
    public class MeleeAttackData : Data
    {
        public Action<Collider, Entity, string> MeleeAttackIsOnAvailableNow;
        public Action <Collider, Entity, string>MeleeAttackIsNotAvailableNow;
    }
}