﻿using System;
using TopAdventure.ECS;

namespace InternalAssets.Scripts.GamePlayScene.DataStore
{
    [Serializable]
    public class DamageData : Data
    {
        public float damage;
    }
}