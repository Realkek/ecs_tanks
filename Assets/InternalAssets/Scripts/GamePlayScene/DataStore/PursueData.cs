﻿using System;
using TopAdventure.ECS;
using UnityEngine;
using UnityEngine.AI;

namespace InternalAssets.Scripts.GamePlayScene.DataStore
{
    [Serializable]
    public class PursueData : Data
    {
        public NavMeshAgent NavMeshAgent;
        public GameObject Pursuer;
        public GameObject Enemy;
    }
}