using InternalAssets.Scripts.GamePlayScene.DataStore;

namespace InternalAssets.Scripts.GamePlayScene.Entity.Characters
{
    using TopAdventure.Unity;
    using UnityEngine;
    using TopAdventure.ECS;

    public class PlayerTank : Entity
    {
        [Foldout("DATA", true)] 
        [SerializeField] private HpData hpData;
        [SerializeField] private DamageData damageData;
        [SerializeField] private MovementData movementData;
        [SerializeField] private PlayerInputData playerInputData;
        private void Awake()
        {
            WriteCollectedData(damageData, hpData, movementData, playerInputData);
            Initialize();
        }
    }
}