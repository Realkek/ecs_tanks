﻿using System.Collections;
using System.Collections.Generic;
using InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses;
using TopAdventure.ECS;
using TopAdventure.Interfaces;
using TopAdventure.Managers;
using UnityEngine;

public class MeleeAttackCollider : MonoBehaviour
{
    private MeleeAttackDataCash meleeAttackDataCash; 
    private Entity _entity;
    private string _currentColliderName;

    private void Start()
    {
        _entity = GetComponentInParent<Entity>();
        _currentColliderName = name;
        meleeAttackDataCash = new MeleeAttackDataCash(_entity.dataList);
    }
    
    private void OnTriggerEnter(Collider otherCollider)
    {
        meleeAttackDataCash.MeleeAttackData.MeleeAttackIsOnAvailableNow?.Invoke(otherCollider, _entity,
            _currentColliderName);
    }

    private void OnTriggerExit(Collider otherCollider)
    {
        meleeAttackDataCash.MeleeAttackData.MeleeAttackIsNotAvailableNow(otherCollider, _entity,
            _currentColliderName);
    }
}