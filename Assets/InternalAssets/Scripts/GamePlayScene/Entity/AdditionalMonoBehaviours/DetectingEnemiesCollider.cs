﻿using System.Collections;
using InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses;
using UnityEngine;

namespace InternalAssets.Scripts.GamePlayScene.Entity.AdditionalMonoBehaviours
{
    using TopAdventure.ECS;

    public class DetectingEnemiesCollider : MonoBehaviour
    {
        [SerializeField] private Entity _entity;
        private DetectingDataCash detectingDataCash;
        private bool _isTriggering;
        private string _currentColliderName;

        private void Start()
        {
            _currentColliderName = name;
            detectingDataCash = new DetectingDataCash(_entity.dataList);
        }

        private void OnTriggerEnter(Collider otherCollider)
        {
            _isTriggering = true;
            detectingDataCash.DetectingData.EnemyHasBeenDetected.Invoke(otherCollider, _entity,
                _currentColliderName);
        }

        private void OnTriggerExit(Collider otherCollider)
        {
            _isTriggering = false;
            StartCoroutine(CountdownAfterTriggerExit(otherCollider));
        }

        IEnumerator CountdownAfterTriggerExit(Collider otherCollider)
        {
            yield return new WaitForSeconds(6);
            if (_isTriggering == false)
                detectingDataCash.DetectingData.EntityDetectingColliderExit.Invoke(otherCollider, _entity,
                    _currentColliderName);
        }
    }
}