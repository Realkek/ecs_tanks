﻿using System.Collections;
using System.Collections.Generic;
using InternalAssets.Scripts.GamePlayScene.DataStore;
using TopAdventure.ECS;
using TopAdventure.Unity;
using UnityEngine;

public class YellowOrk : Entity
{
    [Foldout("DATA", true)] [SerializeField]
    private MovementData movementData;

    [SerializeField]  private HpData hpData;

    private void Start()
    {
        Initialize();
    }
}