﻿using InternalAssets.Scripts.GamePlayScene.Behaviours.CashClasses;
using TopAdventure.ECS;
using UnityEngine;

[CreateAssetMenu(fileName = "Detecting", menuName = "CustomBehaviours/Detecting")]
public class DetectingBehaviour : CustomBehaviour
{
    private DetectingDataCash detectingDataCash;

    protected override void Initialize()
    {
        Subscribe();
        detectingDataCash = new DetectingDataCash(entityInstance.dataList);
    }

    private void RecognitionNearEntities<TEntity, TDetectingEntity, TDetectingCollider>(TEntity otherCollidedEntity,
        TDetectingEntity detectingEntity, TDetectingCollider detectingColliderName)
    {
        var currentColliderName = detectingColliderName as string;
        var otherCollider = (otherCollidedEntity as Collider);
        if (otherCollider != null && otherCollider.name == "Person")
        {
            var entity = (detectingEntity as Entity);
            if (entity != null)
            {
                switch (currentColliderName)
                {
                    case "DetectingCollider":
                        detectingDataCash.DetectingData.EnemyHasBeenDetected?.Invoke(otherCollider, entity,
                            currentColliderName);
                        break;
                    case "MeleeAttackCollider":
                        detectingDataCash.DetectingData.EnemyEnteredTheRadiusOfMeleeAttack?.Invoke(otherCollider,
                            entity, currentColliderName);
                        break;
                }
            }
        }
    }

    private void MissNearEntities<TEntity, TMissingEntity, TDetectingCollider>(TEntity otherCollidedEntity,
        TMissingEntity missingEntity, TDetectingCollider detectingColliderName)
    {
        string currentColliderName = detectingColliderName as string;
        Collider otherCollider = (otherCollidedEntity as Collider);
        if (otherCollider != null && otherCollider.name == "Person")
        {
            Entity entity = (missingEntity as Entity);
            if (entity != null)
            {
                switch (currentColliderName)
                {
                    case "DetectingCollider":
                        detectingDataCash.DetectingData.EnemyHasBeenMissed.Invoke(otherCollider, entity,
                            currentColliderName);
                        break;
                    case "MeleeAttackCollider":
                        detectingDataCash.DetectingData.EnemyEnteredTheRadiusOfMeleeAttack.Invoke(otherCollider, entity,
                            currentColliderName);
                        break;
                }
            }
        }
    }


    public override void Subscribe()
    {
        detectingDataCash.DetectingData.EntityDetectingColliderTriggered += RecognitionNearEntities;
        detectingDataCash.DetectingData.EntityDetectingColliderExit += MissNearEntities;
        detectingDataCash.MeleeAttackData.MeleeAttackIsOnAvailableNow += RecognitionNearEntities;
        detectingDataCash.MeleeAttackData.MeleeAttackIsNotAvailableNow += MissNearEntities;
    }


    public override void UnSubscribe()
    {
        detectingDataCash.DetectingData.EntityDetectingColliderTriggered -= RecognitionNearEntities;
        detectingDataCash.DetectingData.EntityDetectingColliderExit -= MissNearEntities;
        detectingDataCash.MeleeAttackData.MeleeAttackIsOnAvailableNow -= RecognitionNearEntities;
        detectingDataCash.MeleeAttackData.MeleeAttackIsNotAvailableNow -= MissNearEntities;
    }
    
}